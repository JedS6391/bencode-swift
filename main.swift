#!/usr/bin/swift

//
//  main.swift
//  bencode
//
//  Created by Jed Simson on 15/07/16.
//  Copyright © 2016 jedsimson. All rights reserved.
//

import Foundation

if (Process.arguments.count < 2) || (Process.arguments.count > 3) {
    printUsage()

    exit(EXIT_FAILURE)
}

let path = Process.arguments[1]
let flag = Process.arguments.count == 3
let bytes = loadFile(path)


if flag {
    if Process.arguments[2] != "-json" {
        printUsage()
        exit(EXIT_FAILURE)
    }
    else {
        toJSON(bytes)
        exit(EXIT_SUCCESS)
    }
} else {
    printInformation(bytes)
    exit(EXIT_SUCCESS)
}


