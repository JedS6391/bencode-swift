//
//  JSON.swift
//  bencode
//
//  Created by Jed Simson on 17/07/16.
//  Copyright © 2016 jedsimson. All rights reserved.
//

import Foundation


protocol JSONSerializer {
    
    associatedtype T
    func serialize(object: T) -> AnyObject
}

class BEncodeJSONSerializer : JSONSerializer {
    
    typealias T = BEncodedValue
    
    
    private var rawArray: [AnyObject] = []
    private var rawDictionary: [String : AnyObject] = [:]
    private var rawString: String = ""
    private var rawNumber: NSNumber = 0
    private var rawNull: NSNull = NSNull()
    
    func serialize(object: BEncodedValue) -> AnyObject {
        
        switch object {
            case .Dict(_):
                // Serialize to JSON dictionary
                return self.serializeDictionary(object)
            
            case .List(_):
                return self.serializeList(object)
            
            case .Str(_):
                return self.serializeString(object)
    
            case .Number(_):
                return self.serializeNumber(object)
            
            case .Nil:
                return self.rawNull
  
        }
    }
    
    func serializeDictionary(object: BEncodedValue) -> [String:AnyObject] {
        
        var out = self.rawDictionary
        let dict = object.value() as! [String:BEncodedValue]
        
        for (key, value) in dict {
            out[key] = serialize(value)
        }
        
        return out
    }
    
    
    func serializeList(object: BEncodedValue) -> [AnyObject] {
        
        var out = self.rawArray
        let list = object.value() as! [BEncodedValue]
        
        for value in list {
            out.append(self.serialize(value))
        }
        
        return out
    }
    
    func serializeByteArray(bytes: [UInt8]) -> [Int] {
        
        var out = [Int]()
        
        for byte in bytes {
            out.append(Int(byte))
        }
        
        return out
    }
    
    
    func serializeString(object: BEncodedValue) -> AnyObject {
        
        let bytes = object.value() as! [UInt8]
        
        if let bytesAsUTF8String = bytesToUTF8(bytes) {
            return bytesAsUTF8String
        }
        else {
            // If encoding the string as UTF-8 fails, which it likely
            // will when parsing the pieces attribute of the info section
            // (a concatenation of SHA1 hash strings), so we will attempt
            // to construct the hash string (20 byte SHA-1 strings)
            
            var pieces = [NSMutableString]()
            var n = 0
            
            while n < bytes.count {
                let hexString = NSMutableString()
                
                for byte in bytes[n..<n+20] {
                    hexString.appendFormat("%02x", UInt(byte))
                }
                
                pieces.append(hexString)
                n += 20
            }
            
            return pieces
        }
        
    }
    
    func serializeNumber(object: BEncodedValue) -> Int {
        
        let number = object.value() as! Int64
        
        return Int(number)
    }
}