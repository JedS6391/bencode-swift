//
//  bencode.swift
//  bencode
//
//  Created by Jed Simson on 15/07/16.
//  Copyright © 2016 jedsimson. All rights reserved.
//

import Foundation


enum BEncodedValue {
    case Dict(Dictionary<String, BEncodedValue>)
    case List([BEncodedValue])
    case Number(Int64)
    case Str([UInt8])
    case Nil
    
    func value() -> Any? {
        switch self {
            
        case .Dict(let d):
            return d;
            
        case .List(let l):
            return l;
        
        case .Number(let n):
            return n;
            
        case .Str(let s):
            return s;
            
        default:
            return nil;
        }
    }
}

class BEncodingParser {
    
    let dictionaryStart: UInt8  = [UInt8]("d".utf8)[0];      // 100
    let dictionaryTerminator    = [UInt8]("e".utf8)[0];      // 101
    let listStart: UInt8        = [UInt8]("l".utf8)[0];      // 108
    let listTerminator: UInt8   = [UInt8]("e".utf8)[0];      // 101
    let numberStart: UInt8      = [UInt8]("i".utf8)[0];      // 105
    let numberTerminator: UInt8 = [UInt8]("e".utf8)[0];      // 101
    let divider: UInt8          = [UInt8](":".utf8)[0];      // 58
    
    
    func decode(contents: [UInt8]) -> BEncodedValue {
        var generator = contents.generate();
        let current = generator.next()!;
        
        return self.parse(current, generator: &generator);
        
    }
        
    func parse(current: UInt8, inout generator: IndexingGenerator<[UInt8]>) -> BEncodedValue {
        
        switch current {
        case dictionaryStart:
            return BEncodedValue.Dict(self.parseDictionary(&generator));
            
        case listStart:
            return BEncodedValue.List(self.parseList(&generator));
            
        case numberStart:
            return BEncodedValue.Number(self.parseNumber(&generator));
            
        default:
            return BEncodedValue.Str(self.parseByteArray(current, rest: &generator));
        }
        
    }
    
    func parseDictionary(inout contents: IndexingGenerator<[UInt8]>) -> Dictionary<String, BEncodedValue> {
        /*
         *  Parses a BEncoded dictionary from the data.
         */
        
        var dict = [String: BEncodedValue]();
        var keys = [String]();
        
        while var current = contents.next() {
            if current == dictionaryTerminator {
                break;
            }
            
            let data = self.parseByteArray(current, rest: &contents);
            let key = NSString(data: NSData(bytes: data as [UInt8], length: data.count),
                               encoding: NSUTF8StringEncoding) as? String;
            
            current = contents.next()!;
            let value = self.parse(current, generator: &contents);
            
            keys.append(key!);
            dict[key!] = value;
        }
        
        // TODO: Check that keys are sorted according to the raw
        // UTF-8 bytes in the key before returning
        //var sortedKeys = keys.sort({(s: String) -> _ in )
        
        return dict;
    }
    
    func parseList(inout contents: IndexingGenerator<[UInt8]>) -> [BEncodedValue] {
        /*
         * Parses a BEncoded list from the data.
         */
        
        var list = [BEncodedValue]();
        
        while let current = contents.next() {
            if current == listTerminator {
                break;
            }
            
            let value = self.parse(current, generator: &contents);
            list.append(value);
        }
        
        return list;
    }
    
    func parseNumber(inout contents: IndexingGenerator<[UInt8]>) -> Int64 {
        /*
         * Parses a BEncoded number from the data. A number is stored as a long
         * (Int64) due to the fact that it is used to store file size which
         * can exceed INT32_MAX.
         */
        
        var data: [UInt8] = [];
        
        while let current = contents.next() {
            if current == numberTerminator {
                break;
            }
            
            data.append(current);
        }
        
        
        let bytesAsString = NSString(data: NSData(bytes: data as [UInt8], length: data.count),
                                     encoding: NSUTF8StringEncoding) as? String;
        
        return Int64(bytesAsString!)!;
    }
    
    func parseByteArray(first: UInt8, inout rest: IndexingGenerator<[UInt8]>) -> [UInt8] {
        /*
         * Parses a BEncoded string from the data. A string is simply a byte array
         * as it does not have any inherit encoding attached to it. A string has a
         * length component, denoting the length of the byte array, then the byte
         * array itself (i.e. <length>:<contents>).
         */
        
        var lengthData: [UInt8] = [first];
        
        while let current = rest.next() {
            if current == divider {
                break;
            }
            
            lengthData.append(current);
        }
        
        
        let lengthDataBytes = NSData(bytes: lengthData as [UInt8], length: lengthData.count);
        
        // Attempt to get a UTF-8 representation of the string
        let s: NSString? = NSString(data: lengthDataBytes, encoding: NSUTF8StringEncoding)
        
        guard s != nil else {
            return [UInt8]("Unable to decode string length".utf8)
        }
        
        let length = Int(s as! String)
        
        if length == nil {
            // TODO: Handle error while parsing length
        }
        
        var data: [UInt8] = [];
        var cnt = 0;
        
        while cnt < length! {
            let current = rest.next();
            
            data.append(current!);
            
            cnt += 1;
        }
        
        return data;
    }
    
    
}