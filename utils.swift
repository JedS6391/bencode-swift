//
//  utils.swift
//  bencode
//
//  Created by Jed Simson on 15/07/16.
//  Copyright © 2016 jedsimson. All rights reserved.
//

import Foundation

func printUsage() {
    print("USAGE: bencode <torrent_file> [flags]")
    print("Flags:")
    print("\t-json\tExport as JSON")
}

func loadFile(path: String) -> [UInt8] {

    var bytes = [UInt8]();

    if let data = NSData(contentsOfFile: path) {
        var buffer = [UInt8](count: data.length, repeatedValue: 0);

        data.getBytes(&buffer, length: data.length);
        bytes = buffer;
    }

    return bytes
}

func printInformation(bytes: [UInt8]) {
    let be = BEncodingParser()
    let parsed = be.decode(bytes)
    let s = displayDictionary(parsed, indent: 0)

    print(s)
}

func toJSON(bytes: [UInt8]) {
    let be = BEncodingParser()
    let json = BEncodeJSONSerializer()
    let parsed = be.decode(bytes)

    let serialized = json.serialize(parsed) as! NSDictionary

    do {
        let jsonData =  try NSJSONSerialization.dataWithJSONObject(serialized,
                                                                   options: NSJSONWritingOptions.PrettyPrinted)

        print(NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String)
    } catch let e as NSError {
        print(e)
    }
}


func displayList(list: [BEncodedValue]) -> String {
    var listString = "["
    var count = 0


    for obj in list {
        /* We assume that a BEncoded List will generally be one of three forms:
         *
         *   1. A list of list of strings, to account for an announce-list
         *      as per BEP 12: http://bittorrent.org/beps/bep_0012.html
         *
         *   2. A list of dictionaries, to account for multiple file mode
         *      as per https://wiki.theory.org/BitTorrentSpecification#Info_in_Multiple_File_Mode
         *
         *   3. A list of strings, as a general case and to account for
         *      the path attribute of a dictionary encountered in type 2,
         *      as per https://wiki.theory.org/BitTorrentSpecification#Info_in_Multiple_File_Mode
         */

        let separator = (count + 1) == list.count ? "" : ", "

        // Case 1 - List of list of strings
        if case .List(let l) = obj {
            listString += displayList(l) + separator
        }

        // Case 2 - List of dictionaries
        if case .Dict(_) = obj {

            // Because we can make assumptions about where a list of
            // dictionaries will likely occur (the info section of the
            // torrent), we can format it accordingly, as we can assume
            // that the info section is a nested dictionary and will
            // be printed at indent level 1, so to format everything nicely,
            // we should print at indent level 2.

            // TODO: It might be nice to create a JSON representation and
            // simply pretty-print that?
            listString += "\n" + displayDictionary(obj, indent: 2)
        }

        if case .Str(let bytes) = obj {
            if let bytesAsUTF8String = bytesToUTF8(bytes) {
                listString += bytesAsUTF8String + separator
            }
            else {
                // Some error decoding string - shouldn't happen
                // but if it does we'll just ignore it for now...
                continue
            }
        }

        count += 1
    }

    return listString + "]"
}


func displayDictionary(info: BEncodedValue, indent: Int) -> String {
    var dictString = ""
    let padding = "".stringByPaddingToLength(4 * indent,
                                             withString: " ",
                                             startingAtIndex: 0)

    if case .Dict(_) = info {} else {
        print("Cannot display information for non-dictionary BEncodedValue - aborting!")
        exit(EXIT_FAILURE)
    }

    let torrent = info.value() as! [String:BEncodedValue]


    for (key, value) in torrent {

        switch value {
            case .Dict(_):
                dictString += "\(padding)\(key) => \n"

                dictString += displayDictionary(value, indent: indent+1)

            case .List(let l):
                dictString += "\(padding)\(key) => \(displayList(l))\n"

            case .Number(let n):
                dictString += "\(padding)\(key) => \(n)\n"

            case .Str(let bytes):
                if let bytesAsUTF8String = bytesToUTF8(bytes) {
                    dictString += "\(padding)\(key) => \(bytesAsUTF8String)\n"
                }
                else {
                    dictString += "\(padding)\(key) => \(bytes)\n"
                }

            default:
                print("Unexpected type <\(key), \(value)> encountered - aborting!")
                exit(EXIT_FAILURE)
        }
    }

    return dictString
}


// Useful method for converting a BEncoded byte array to
// an NSString (UTF-8 encoded)
func bytesToUTF8(bytes: [UInt8]) -> String? {

    if bytes.count == 0 {
        return nil;
    }

    let s = NSString(bytes: bytes,
                     length: bytes.count,
                     encoding: NSUTF8StringEncoding);

    if s != nil {
        return String(s!);
    }

    return nil


}
