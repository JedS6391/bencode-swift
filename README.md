# Bencode Parser/Serializer

---

A small little project to load .torrent files, parse the bencoded format of the file into an internal representation and allow for nice printing of information, or output to JSON.